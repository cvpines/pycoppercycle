CopperCycle: Cross-Platform Tools for Text Generation
=====================================================

.. image:: img/header.png
    :width: 100%
    :align: center
    :alt: CopperCycle banner

CopperCycle is a cross-platform library for generating text using grammars.

Installation is simple::

    $ pip install pycoppercycle

CopperCycle was created by Coriander V. Pines and is available under
the BSD 3-Clause License.

The source is available on
`GitLab <https://gitlab.com/cvpines/pycoppercycle/>`_.
