![CopperCycle banner](docs/source/img/header.png "CopperCycle")

# CopperCycle: Cross-Platform Tools for Text Generation

[![pipeline status](https://gitlab.com/cvpines/pycoppercycle/badges/master/pipeline.svg)](https://gitlab.com/cvpines/pycoppercycle/-/commits/master)
[![coverage report](https://gitlab.com/cvpines/pycoppercycle/badges/master/coverage.svg)](https://gitlab.com/cvpines/pycoppercycle/-/commits/master)
[![PyPI](https://img.shields.io/pypi/v/pycoppercycle)](https://pypi.org/project/pycoppercycle/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/pycoppercycle)]((https://pypi.org/project/pycoppercycle/))
[![PyPI - License](https://img.shields.io/pypi/l/pycoppercycle)](https://gitlab.com/cvpines/pycoppercycle/-/blob/master/LICENSE)

CopperCycle is a cross-platform library for generating text using grammars.
